# Muscula Java Logback Appender

## Configuration

1. Add dependency to pom file
```xml
<dependency>
    <groupId>com.muscula</groupId>
    <artifactId>muscula-logback</artifactId>
    <version>1.0.0</version>
</dependency>
```

1. Configure logback

```xml
<configuration>
    <appender name="muscula" class="com.muscula.logback.appender.MusculaAppender">
        <logId>YOUR_MUSCULA_LOG_ID</logId>
    </appender>
    <root level="info">
        <appender-ref ref="muscula"/>
    </root>
</configuration>
```
