package com.muscula.logback.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.AppenderBase;
import com.muscula.core.Credentials;
import com.muscula.core.Logs;
import com.muscula.core.model.MusculaLog;

public class MusculaAppender extends AppenderBase<ILoggingEvent> {

    private String logId;
    private String endpointUrl;

    protected void append(ILoggingEvent logEvent) {
        Credentials.setLogId(logId);
        Credentials.setEndpointUrl(endpointUrl);
        Throwable throwable = logEvent.getThrowableProxy() == null ? null : ((ThrowableProxy)logEvent.getThrowableProxy()).getThrowable();
        Logs.push(new MusculaLog(logEvent.getMessage(), logEvent.getLevel().toString(), throwable, logEvent.getArgumentArray()));
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

    public void setEndpointUrl(String endpointUrl) {
        this.endpointUrl = endpointUrl;
    }
}
